using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
//public class objetoAbrir : interagir
public class objetoAbrir : MonoBehaviour
{
    public Sprite aberto;
    public Sprite fechado;

    private SpriteRenderer sr;
    private bool estaAberto;

    /*public override void interagir(){
        #if(estaAberto)
            // sr.sprite = fechado;
        #else
            // sr.sprite = aberto;
        // Altera o sprite do objeto quando tiver um
        #estaAberto = !estaAberto;
    #}*/

    private void Start(){
        sr = GetComponent<SpriteRenderer>();
        sr.sprite = fechado;
    }
}
