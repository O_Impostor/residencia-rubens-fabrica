using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public abstract class interagivel : MonoBehaviour
{
    private void Reset(){
        GetComponent<BoxCollider2D>().isTrigger = true;
    }
    public abstract void interagir();

    private void OnTriggerEnter2D(Collider2D collision){
        if(collision.CompareTag("player"))
            collision.GetComponent<playerMovimento>().interagirIconeAberto();
    }

    private void OnTriggerExit2D(Collider2D collision){
        if(collision.CompareTag("player"))
            collision.GetComponent<playerMovimento>().interagirIconeFechado();
    }
        
}
