using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerMovimento : MonoBehaviour
{
    public Animator anim;
    public float speed;
    public GameObject interagirIcone;

    private Vector2 caixaTamanho = new Vector2(0.1f, 1f);

    // Update is called once per frame
    void Update()
    {
        //if(Input.GetKeyDown(KeyCode.Z))
            //checarInteracao();

        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0f);

        anim.SetFloat("Horizontal", movement.x);
        anim.SetFloat("Vertical", movement.y);
        anim.SetFloat("Speed", movement.magnitude);

        transform.position = transform.position + movement * speed * Time.deltaTime;
    }

    public void interagirIconeAberto(){
        interagirIcone.SetActive(true);
    }
    public void interagirIconeFechado(){
        interagirIcone.SetActive(false);
    }

    /*private void checarInteracao(){
        RaycastHit2D[] hits = Physics2D.BoxCastAll(transform.position, caixaTamanho, 0, Vector2.zero);

        if(hits.Lenght > 0){ // Checaria se o player está colidindo com algum objeto
            foreach(RaycastHit2D rc in hits){
                if(rc.transform.GetComponent<interagivel>()){ // Checaria se o objeto possui uma opção de interagir
                    (rc.transform.GetComponent<interagivel>()).interagir(); 
                    return;
                }
            }
        } 
    } */
}
